# Spaceships War
## Author: Luis Enrique Arias Serrano
**Spaceships War** is a game based on spaceships that have to destroy and dodge obstacles to overcome the levels.
You can play the game <a href="https://luiseariass.gitlab.io/proyecto1-spaceshipswar/">here</a>

<img src = "imagenes/pantalla-principal.png" width="600">



About the game
============
* In single player mode: You start out as an intergalactic warrior, you have to rescue Princess Zoé from the hands of the intergalactic commander.

* In multiplayer mode: you have to compete with the other player for the heart of Princess Zoé, whoever wins the intergalactic race can marry her and rule alongside the princess.

* Rules: 
    You must destroy all the missiles.
    You cannot destroy the planets.
If you destroy the asteroid you will arrive at the commander much sooner.	    


To do list
============

* Add power ups.
* Increase animation of assets
* In single player mode: add more game levels


Credits
============

* KAVAK Intern Program 
* Algorithm for the animation of the game storytelling. <a href="https://devcode.la/">Devcode</a> 
* <a href="https://www.google.com/">Google</a>