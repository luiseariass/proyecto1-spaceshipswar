/*
  En este archivo de JS esta la animacion de la historia de inicio 
  de cada modo de juego, este codigo es una adaptación al hecho por
  https://devcode.la/
 */

let w = window.innerWidth;
let h = window.innerHeight;
let intro = document.getElementsByClassName("intro")[0];
let historia = document.getElementsByClassName("historia")[0];
let párrafos = document.getElementsByClassName("párrafos")[0];
let intervalHistoria = 0;
let clickOnStart = 1;

intro.style.fontSize = w / 30 + "px";
historia.style.fontSize = w / 20 + "px";
párrafos.style.height = h + "px";

window.addEventListener("resize", function() {  
  w = window.innerWidth;
  h = window.innerHeight;
  intro.style.fontSize = w / 30 + "px";
  historia.style.fontSize = w / 20 + "px";
  párrafos.style.height = h + "px";
});

function animar() {
  intro.className = 'intro texto_intro animación_intro';
  historia.className = 'historia texto_historia animación_historia';
  if (clickOnStart ===1){
    intervalHistoria=setInterval(clickButton, 20500); 
    clickOnStart++
  }
  

}
/**
 Funcion que llama al juego despues de la animacion inicial.
 */
function clickButton() { 
    console.log('gege')
    if (document.querySelector('.p1') === null){
        document.querySelector('.p2').click(); 
    } else {
        document.querySelector('.p1').click(); 
    }
} 

