/**
 *Clase que dibuja el board en el canvas
 * @class Board
 */
class Board {
    /**
     *Recibe como parametro el canvas en donde se dibujara el background
     * @param {*} context
     * @memberof Board
     */
    constructor(context) {
      this.context = context
      this.x = 0;
      this.y = 50;
      this.width = canvas.width;
      this.height = canvas.height;
      this.img = new Image();
      this.img.src = "imagenes/background.png";
      this.img.onload = () => {
        this.draw();
      }
    }
  
    draw() {
      this.y+=5
      if(this.y > canvas.height){
        this.y = 50
      }  
      this.context.drawImage(this.img, this.x, this.y , canvas.width, canvas.height);
      this.context.drawImage(this.img, this.x, this.y - canvas.height,canvas.width,canvas.height)
    }
}
/**
 *Intancia que controla el banner de progreso y vida de cada jugador
 * @class Banner
 */
class Banner{
  /**
   * @param {*} context
   * recibe el canvas en donde se pintara el banner
   * @memberof Banner
   */
  constructor(context){
    this.context = context
    this.score = 5;
    this.x = 0;
    this.y = 0;
    this.width = canvas.width;
    this.height = 50
    this.imgFull = new Image();
    this.imgFull.src = "imagenes/hearts/heart_full.png";
    this.imgEmpty = new Image();
    this.imgEmpty.src = "imagenes/hearts/heart_empty.png";
    this.imgBarTable = new Image();
    this.imgBarTable.src = "imagenes/bar/Table.png"
    this.imgBarProgress = new Image();
    this.imgBarProgress.src = "imagenes/bar/Loading_Bar_2_2.png"
    
  }
  draw() {
    this.context.fillStyle = '#494D4C';
    this.context.fillRect(this.x,this.y, this.width, this.height)
    this.context.fill();
    this.context.font = '35px Helvetica';
    this.context.fillStyle='white'
    this.context.fillText('HP:', 25, 40);
    this.context.drawImage(this.imgBarTable,440,this.y+20,200,20)
    switch (this.score){
      case 5 :
        for (let i= 0;i<5;i++){
          this.context.drawImage(this.imgFull,90+i*30,17.5,25,25)
        }
        break; 
      case 4 :
        for (let i= 0;i<4;i++){
          this.context.drawImage(this.imgFull,90+i*30,17.5,25,25)
        }
        for (let i= 4;i>3;i--){
          this.context.drawImage(this.imgEmpty,90+i*30,17.5,25,25)
        }
        break;
        case 3 :
        for (let i= 0;i<3;i++){
          this.context.drawImage(this.imgFull,90+i*30,17.5,25,25)
        }
        for (let i= 4;i>2;i--){
          this.context.drawImage(this.imgEmpty,90+i*30,17.5,25,25)
        }
        break;
        case 2 :
        for (let i= 0;i<2;i++){
          this.context.drawImage(this.imgFull,90+i*30,17.5,25,25)
        }
        for (let i= 4;i>1;i--){
          this.context.drawImage(this.imgEmpty,90+i*30,17.5,25,25)
        }
        break;
        case 1 :
        for (let i= 0;i<1;i++){
          this.context.drawImage(this.imgFull,90+i*30,17.5,25,25)
        }
        for (let i= 4;i>0;i--){
          this.context.drawImage(this.imgEmpty,90+i*30,17.5,25,25)
        }
        break;     
      }
  }
  updateHP() {
    this.score--
  }
  updateProgress(progress){
    this.context.drawImage(this.imgBarProgress,441,this.y+21,progress,17)
  }
  /**
   * Metodo encargado de reiniciar el interval del juego
   *
   * @memberof Banner
   */
  restart(){
    clearInterval(interval);    
  }
}
class Spaceship {
    /**
     *Creates an instance of Spaceship.
     * @param {*} context
     * @param {*} player
     * recibe el canvas y el tipo de jugador a pintar, ya que cada jugador tiene su nave predeterminada
     * @memberof Spaceship
     */
    constructor(context,player){
        this.context = context;
        this.x = canvas.width/2 - canvas.width*0.05;
        this.y = canvas.height*0.875 ;
        this.life = 5;
        this.width = Math.floor(canvas.width*0.1);
        this.height = Math.floor(canvas.height*0.125); 
        this.img = new Image();
        this.img.src = `imagenes/naves/${player}.png`;
        this.img.onload = () => {
        this.draw();
      }
    }
  
    /**
     *Este metodo ademas de dibujar a la nave sobre el canvas
     *controla que la nave siempre se dibuje sobre el canvas
     * @memberof Spaceship
     */
    draw() {
        if (this.x>0 && this.x < canvas.width - this.width){
          this.x=this.x
        }else if (this.x <= this.width){
            this.x = 0.1
        }else{
            this.x = canvas.width - this.width*1.00000001
        }

        if (this.y + this.height*0.989999999999< canvas.height && this.y >canvas.height/2){
          this.y = this.y
        }
        else if (this.y + this.height>canvas.height){
          this.y =canvas.height - this.height*1.01000;

        }else if (this.y < canvas.height/2){
          this.y = (canvas.height/2)*1.000001
        }
        this.context.drawImage(this.img, this.x, this.y, this.width, this.height);
    }
    moveLeft(){
        this.x -= 25
    }
    moveRight(){
        this.x += 25
    }
    moveFoward(){
        this.y +=12.5;
      } 
    moveBack(){
        this.y-=12.5;
    } 
    isTouching(obstacle,cicle=0) {
        if (cicle){
          return (
            this.x < obstacle.x + 1.6*obstacle.radius &&
            this.x + 0.9*this.width > obstacle.x &&
            this.y < obstacle.y + 1.2*obstacle.radius &&
            this.y + this.height > obstacle.y
          );
        }else{
          return (
            this.x < obstacle.x + obstacle.width &&
            this.x + this.width > obstacle.x &&
            this.y < obstacle.y + 0.95*obstacle.height &&
            this.y + 0.95*this.height > obstacle.y
          );
    
        }
        
      }
    restart(){
      this.x = canvas.width/2 - canvas.width*0.05;
      this.y = canvas.height*0.875 ;
      this.life = 5;

    }  

}

/**
 *Crea la nave del obstaculo final
 *
 * @class Commandant
 * @extends {Spaceship}
 */
class Commandant extends Spaceship {
  constructor (context,player){
    super(context,player)
    this.y = 50;
    this.width = Math.floor(canvas.width*0.15);
    this.height = Math.floor(canvas.height*0.15); 
  }
  draw() {
    if (this.x>0 && this.x < canvas.width - this.width){
      this.x=this.x
    }else if (this.x <= this.width){
        this.x = 0.1
    }else{
        this.x = canvas.width - this.width*1.00000001
    }

    if (this.y>50 && this.y < canvas.height/2 - this.height){
      this.y=this.y
    }else if (this.y <= 50){
        this.y = 50
    }else{
        this.y = canvas.height/2 - this.height
    }
    this.context.drawImage(this.img, this.x, this.y, this.width, this.height);
  }
  moveLeft(){
    this.x -= 30
  }
  moveRight(){
    this.x += 30
  }
  moveFoward(){
    this.y +=15;
  } 
  moveBack(){
    this.y-=15;
  }
  restart(){
    this.y = 50;

  } 
}


/**
 *
 * dibuja los disparos de cada player
 * @class Fire
 */
class Fire {
  constructor(context,x,y){
    this.context = context
    this.x = x
    this.y = y
    this.width = Math.floor(canvas.width*0.075);
    this.height = Math.floor(canvas.height*0.075); 
    this.img = new Image();
    this.img.src = "imagenes/Projectiles/Laser01.png"
  }
    draw() {
      this.y-=4
      if (this.y > 35){
        this.context.drawImage(this.img, this.x, this.y, this.width, this.height);
      }
      
    }
    isTouching(obstacle,estado=1) {
      if (estado){
        return (
          this.x < obstacle.x + obstacle.width &&
          this.x + this.width > obstacle.x &&
          this.y < obstacle.y + obstacle.height &&
          this.y + this.height > obstacle.y
        );
      } else{
        return (
          this.x < obstacle.x + 3*obstacle.width/5 &&
          this.x > obstacle.x &&
          this.y + 3*this.height/5< obstacle.y + obstacle.height &&
          this.y > obstacle.y
        );
      } 
      

    }
}


class BadFire extends Fire {
  constructor(context,x,y){
    super(context,x,y)
    this.width = Math.floor(canvas.width*0.1);
    this.height = Math.floor(canvas.height*0.25); 
    this.img.src = "imagenes/Projectiles/laserBullet.png"

  }
  draw() {
    this.y+=4
    if (this.y < canvas.height){
      this.context.drawImage(this.img, this.x, this.y, this.width, this.height);
    }
    
}
isTouching(obstacle) {
        
  return (
    this.x < obstacle.x + obstacle.width &&
    this.x + this.width/3 > obstacle.x &&
    this.y < obstacle.y + obstacle.height &&
    this.y + this.height/4 > obstacle.y
  );

}
}

/**
 *
 *Intancia que genera los obstaculos
 * @class Obstacle
 */
class Obstacle {
  /**
   *La posicion en y siempre es la misma y recibe la posicion en x y el width y el height
   * @param {*} context
   * @param {*} x
   * @param {*} width
   * @param {*} height
   * @memberof Obstacle
   */
  constructor(context,x,width,height) {
    this.context = context
    this.x = x;
    this.y = 50;
    this.width = width;
    this.height = height;
    this.img = new Image();
  }
  draw() {
    this.y++
    this.context.drawImage(this.img, this.x, this.y, this.width, this.height);
  }
}
/**
 * Cada herencia dibuja un obstaculo distinto, se tienen varias imagenes y la eleccion de la imagen se hace de manera aleatoria
 */

class Asteroid extends Obstacle {
  constructor(context,x,width,height){
    super(context,x,width,height)
    this.radius = (width**2)/(8*height) + (height/2)
    this.img.src = "imagenes/meteoritos/nivel_1/Asteroids_32x32_00"+Math.ceil(Math.random()*8)+".png"
  }
}
class Planet extends Obstacle {
  constructor(context,x,width,height){
    super(context,x,width,height)
    this.radius = (width**2)/(8*height) + (height/2)
    this.img.src = "imagenes/Planetas/"+Math.ceil(Math.random()*3)+".png"
  }
}
class Missile extends Obstacle {
  constructor(context,x,width,height){
    super(context,x,width,height)
    this.img.src = "imagenes/Missile_"+Math.ceil(Math.random()*3)+".png"
  }
  isTouching(obstacle) {
    
      return (
        this.x < obstacle.x + obstacle.width &&
        this.x + this.width > obstacle.x &&
        this.y < obstacle.y + obstacle.height &&
        this.y + this.height > obstacle.y
      );
  }

}
/**
 * Las siguientes intancias controlan los niveles del juego que iran cambiando dependiendo del
 * progreso de cada jugador. Cada instancia tiene su comportamiento particular.
 */
class NivelObstacle {
  constructor(){
      this.frame = Math.round(Math.random()*10) + 195
      this.width = 70;
      this.height = 70;
      this.progressLimit = 20;         
    }
    update() {
      this.frame-=Math.round(Math.random()*5) + 10;
      this.width*=0.925;
      this.height*=0.925;
      this.progressLimit+=20;
    }
    restart(){
      this.frame = Math.round(Math.random()*10) + 195
      this.width = 70;
      this.height = 70;
      this.progressLimit = 20;  
    }
}

class NivelPlanet extends NivelObstacle{
  constructor(){
    super()
    this.frame = Math.round(Math.random()*10) + 345;
    this.width = 65;
    this.height = 50;
    this.progressLimit = 40;
  }
  update() {
    this.frame-=Math.round(Math.random()*5) + 25;
    this.width*=1.125;
    this.height*=1.125;
    this.progressLimit+=40;
  }
  
  restart(){
    this.frame = Math.round(Math.random()*10) + 345;
    this.width = 65;
    this.height = 50;
    this.progressLimit = 40;
  }
}

class NivelMissile extends NivelObstacle{
  constructor(){
    super()
    this.frame =Math.round(Math.random()*10) + 445;
    this.width = 35;
    this.height = 125;
    this.progressLimit = 50;
  }
  update() {
    this.frame-=Math.round(Math.random()*15) + 40;
    this.width*=0.85;
    this.height*=0.85;
    this.progressLimit+=50;
  }
  restart(){
    this.frame =Math.round(Math.random()*10) + 445;
    this.width = 35;
    this.height = 130;
    this.progressLimit = 50;
  }
}


/**
 *Genera la porción en donde se generar el obstaculo de manera aleatoria.
 *El canvas se porciona en tres y no se puede dibujar en la misma porcion del canvas
 *dos veces seguidas
 * @class State
 */
class State {
  constructor(){
    this.state = Math.floor(Math.random()*3)
    this.border = [0,2];
  }
  generateState (width){
    switch (this.state){
      case 0 :
        this.state = Math.round(Math.random()) + 1
        return Math.floor(Math.random()*((canvas.width/3)-2*width)) + width
        break;
      case 1 : 
        this.state = this.border[Math.round(Math.random())]
        return Math.floor(Math.random()*((canvas.width/3)-2*width)) + width + canvas.width/3
        break;
      case 2 :
        this.state = Math.round(Math.random())
        return Math.floor(Math.random()*((canvas.width/3)-2*width)) + width + 2*canvas.width/3
        break;
    } 
  }
}
class Explosion {
  constructor(context,x,y,width,height){
    this.context = context
    this.x = x
    this.y = y
    this.width = width*1.5
    this.height = height*1.5 
    this.img = new Image();
    this.img.src = "imagenes/Projectiles/Blaster05.png"
  }
  draw(){
    this.width-=2
    this.height-=2
    this.x ++
    this.y ++
    if (this.width > 0 && this.height>0){
      this.context.drawImage(this.img, this.x, this.y, this.width, this.height);
    }
    

  }
}