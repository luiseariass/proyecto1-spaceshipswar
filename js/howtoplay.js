/* Maneja la interaccion de la pagina de inicio con el boton
 * que dice como se debe jugar
 */
const index = document.querySelector("body");
let title = document.querySelector("#title")
let botones = document.querySelector("#buttons")
let howToPlay = document.querySelector("#how-to-play")
howToPlay.style.display="none"

window.onload = () => {

    index.addEventListener('click', e => {
          if(e.target.classList.contains('control')){
            howToPlay.style.display="block"
            title.style.display="none"
            botones.style.display="none"
          }else if(e.target.classList.contains('go-back')){
            howToPlay.style.display="none"
            title.style.display="block"
            botones.style.display="block"
          }
    })
}  
