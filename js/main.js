const initGame = document.querySelector("body");
const options =document.querySelector("#options")
let canvas = document.getElementById('canvasp1')
let canvasp2 = document.getElementById('canvasp2') 
let ctx = canvas.getContext('2d')
let ctxP2 = canvasp2.getContext('2d')
let startp1 = document.querySelector("#story-p1")
let startp2 = document.querySelector("#story-p2")
let isPause = 0;
if (startp2 === null){
  startp1.style.display = "block"
}else{
  startp2.style.display = "block"
}

canvas.style.display    = "none";
canvasp2.style.display = "none"
options.style.display = "none"

window.onload = () => {

  initGame.addEventListener('click', e => {
        if(e.target.classList.contains('p1')){
            clearInterval(intervalHistoria)
            canvas.style = "position:absolute; left: 40%; margin-left: -200px;";
            startp1.style.display = "none"
            canvas.style.display = "block";
            startGamep1();
        }else if (e.target.classList.contains('p2')){
            clearInterval(intervalHistoria)
            startp2.style.display = "none"
            canvasp2.style.display = "block"
            canvas.style.display = "block";
            startGamep2();
        }else if(e.target.classList.contains('restart-p2')){
          clearInterval(intervalHistoria)
          restart()
          startGamep2();
        }else if (e.target.classList.contains('restart-p1')){
          clearInterval(intervalHistoria)
          restart()
          startGamep1();
        }
  })
  
  /**
   *Las dos funciones siguiente manejan el motor del juego
   *y la funcionalidad de cada boton en la jugabilidad del juego
   */
  function startGamep1(){
    interval = setInterval(updatep1, 1000 / 60);
    document.onkeydown = e => {
      e.preventDefault();
        switch (e.keyCode) {
          case 65:
            spaceship.moveLeft()
            return
          case 83:
            spaceship.moveFoward()
            return
          case 68:
            spaceship.moveRight()
            return;
          case 87:
            spaceship.moveBack()
            return;
          case 32 : 
            fires.push(new Fire(ctx,spaceship.x + spaceship.width/8,spaceship.y - spaceship.height/4))
            return;
          case 27 : 
            pause(updatep1)
            return;

        }
      }
  };
  function startGamep2(){
    interval = setInterval(updatep2, 1000 / 60);
    clearInterval(intervalHistoria)
    document.onkeydown = e => {
      e.preventDefault();
        switch (e.keyCode) {
          case 65:
            spaceship.moveLeft()
            return
          case 83:
            spaceship.moveFoward()
            return
          case 68:
            spaceship.moveRight()
            return;
          case 87:
            spaceship.moveBack()
            return;
          case 32 : 
            fires.push(new Fire(ctx,spaceship.x + spaceship.width/8,spaceship.y - spaceship.height/4))
            return;
          case 37:
            spaceshipP2.moveLeft()
            return
          case 40:
            spaceshipP2.moveFoward()
            return
          case 39:
            spaceshipP2.moveRight()
            return;
          case 38:
            spaceshipP2.moveBack()
            return;
          case 13 : 
            firesP2.push(new Fire(ctxP2,spaceshipP2.x + spaceshipP2.width/8,spaceshipP2.y - spaceshipP2.height/4))
            return;
          case 27 : 
            pause(updatep2)
          return;
        }
      }
  };
}
/**
 * restablece todos a sus valores iniciales
 */
function restart(){
  clearInterval(interval)
  frames = 0;
  isPause = 0;
  interval = 0;
  asteroid = [[],[]];
  planet = [[],[]];
  missile = [[],[]];
  fires = [];
  firesP2 = [];
  badFire =[];
  badFireP2 =[];
  colision = [];
  progress = 0;
  progressP2 = 0;
  game = 1;
  gameP2 = 1;
  spaceship.restart()
  spaceshipP2.restart()
  bannerP2.score =5;;
  banner.score = 5;
  nivelAsteroid.restart()
  nivelPlanet.restart()
  nivelMissile.restart()
  nivelAsteroidP2.restart()
  nivelPlanetP2.restart()
  nivelMissileP2.restart()
  badGuy.restart()
  badGuyP2.restart()
  options.style.display   = "none";
}

/**
 *Maneja las pausas del juego
 *La pausa del juego cambia dependiendo el modo de juego que este sucediendo
 *Por eso se ingresa como paramentro el modo de juego en el que este.
 * @param {*} modeGame
 */
function pause(modeGame){
  if (isPause){
    interval = setInterval(modeGame, 1000 / 60);
    options.style.display   = "none";
    isPause=0;
  }else{
    clearInterval(interval)
    if (modeGame === updatep1){
      options.style = "position:absolute; left: 44%; margin-left: -200px;";
    }
    options.style.display   = "block";
    isPause=1;
  }

}


