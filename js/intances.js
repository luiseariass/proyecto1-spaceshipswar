/**
 * Script que llama a todas las intancias que se usaran
 * para que el juego funcione de una manera adecuada.
 */

const board = new Board(ctx);
const boardP2 = new Board (ctxP2)
const spaceship = new Spaceship(ctx,'p1');
const spaceshipP2 = new Spaceship (ctxP2,'p2')
const nivelAsteroid = new NivelObstacle();
const nivelPlanet = new NivelPlanet();
const nivelMissile = new NivelMissile();
const nivelAsteroidP2 = new NivelObstacle();
const nivelPlanetP2 = new NivelPlanet();
const nivelMissileP2 = new NivelMissile();
const banner = new Banner(ctx);
const bannerP2 = new Banner(ctxP2);
const state = new State();
const stateP2 = new State();
const badGuy = new Commandant(ctx,'bad1');
const badGuyP2 = new Commandant(ctx,'bad2');
