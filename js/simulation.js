/**
 * Maneja la creacion de todas las funciones para que el juego funciones
 */

let frames = 0;
let interval = 0;
let asteroid = [[],[]];
let planet = [[],[]];
let missile = [[],[]];
let fires = [];
let firesP2 = [];
let badFire =[];
let badFireP2 =[];
let colision = [];
let progress = 0;
let progressP2 = 0;
let moves = ['moveLeft','moveBack','moveFoward','moveRight','moveRight','moveLeft']
let game = 1;
let gameP2 = 1;

/**
 * Motor de juego para modo un jugador
 * Llama a todas la funciones que generar los objectos deljuego
**/
/**
 *
 *
 */
function updatep1() {
    frames++;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (game===1){
      board.draw();
      spaceship.draw();
      banner.draw();
      //Si el progreso del jugador no ha pasado un limite se seguiran generando y dibujando los obstaculos
      //cuando se llega al limite se dejan de generar los obstaculos y solo se seguiran dibujando los que ya estaban generando
    if (progress<180){
      generateAsteroid(ctx,state,0);
      drawAsteroid();
      generatePlanet(ctx,state,0);
      drawPlanet();
      generateMissile(ctx,state,0);
      drawMissile();
      drawFires();
      checkCollitions();
      drawExplosion();
      checkGameOver();
      banner.updateProgress(progress)
      //Cuando se llega un cierto nivel de progreso
      //se aumenta la dificultad
      //para cada obstaculo se tiene un limite de progreso distinto
      if (nivelPlanet.progressLimit<progress){
        nivelPlanet.update();
      }
      if (nivelAsteroid.progressLimit<progress){
        nivelAsteroid.update();
      }
      if (nivelMissile.progressLimit<progress){
        nivelMissile.update();
      }
      if (frames % 75 === 0){
        progress++
        banner.updateProgress(progress)
      }
    }else{
      badGuy.draw();
      moveBadGuy();
      drawAsteroid();
      drawPlanet();
      drawMissile();
      drawFires();
      checkCollitions();
      checkCollitionsNaves();
      drawExplosion();
      banner.updateProgress(progress)
      checkGameOver();
      checkWin();
    }
    //Se despliegan imagenes estaticas cuando perdiste o ganaste, y se desplegan los botones de menu y restart
  }else if (game===0){
    let imagen = new Image();
    imagen.src = "imagenes/gameover.jpg";
    ctx.drawImage(imagen, 0, 0, canvas.width,canvas.height);
    options.style.display="block"
    options.style = "position:absolute; left: 44%; margin-left: -200px;";
  }else{
    let imagen = new Image();
    imagen.src = "imagenes/winner.jpg";
    ctx.drawImage(imagen, 0, 0, canvas.width,canvas.height);
    options.style.display="block"
    options.style = "position:absolute; left: 44%; margin-left: -200px;";
  }  
}

/**
 * Motor de juego para modo dos jugadores
 * Llama a todas la funciones que generar los objectos deljuego
 */
function updatep2() {
    frames++;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctxP2.clearRect(0, 0, canvas.width, canvas.height);
    if (game===1 && gameP2===1){
        board.draw();
        boardP2.draw();
        spaceship.draw();
        spaceshipP2.draw();
        banner.draw();
        bannerP2.draw();
        if (progress<180 && progressP2<180){
            generateAsteroid(ctx,state,0);
            generateAsteroid(ctxP2,stateP2,1,nivelAsteroidP2);
            drawAsteroid();
            generatePlanet(ctx,state,0);
            generatePlanet(ctxP2,stateP2,1,nivelPlanetP2,nivelMissileP2,spaceshipP2);
            drawPlanet();
            generateMissile(ctx,state,0);
            generateMissile(ctxP2,stateP2,1,nivelMissileP2);
            drawMissile();
            drawFires();
            checkCollitions();
            checkCollitionsP2();
            drawExplosion();
            checkGameOver();
            banner.updateProgress(progress)
            bannerP2.updateProgress(progressP2)
        if (nivelPlanet.progressLimit<progress){
            nivelPlanet.update();
        }
        if (nivelAsteroid.progressLimit<progress){
            nivelAsteroid.update();
        }
        if (nivelMissile.progressLimit<progress){
            nivelMissile.update();
        }
        if (nivelPlanetP2.progressLimit<progressP2){
            nivelPlanetP2.update();
        }
        if (nivelAsteroidP2.progressLimit<progressP2){
            nivelAsteroidP2.update();
        }
        if (nivelMissileP2.progressLimit<progressP2){
            nivelMissileP2.update();
        }
        if (frames % 75 === 0){
            progress++
            progressP2++
            banner.updateProgress(progress)
            bannerP2.updateProgress(progressP2)

        }
    }else if (progressP2>=180 && progress>=180){
      badGuy.draw();
      badGuyP2.draw();
      moveBadGuy();
      drawAsteroid();
      drawPlanet();
      drawMissile();
      drawFires();
      checkCollitions();
      checkCollitionsP2();
      checkCollitionsNaves();
      checkCollitionsNavesP2();
      drawExplosion();
      banner.updateProgress(progress)
      bannerP2.updateProgress(progressP2)
      checkGameOver();
      checkWin();
    }else if (progressP2>=180 && progress<180){
        badGuyP2.draw();
        moveBadGuy();
        generateAsteroid(ctx,state,0);
        drawAsteroid();
        generatePlanet(ctx,state,0);
        drawPlanet();
        generateMissile(ctx,state,0);
        drawMissile();
        drawFires();
        checkCollitions();
        checkCollitionsP2();
        checkCollitionsNaves();
        checkCollitionsNavesP2();
        drawExplosion();
        checkGameOver();
        checkWin();
        banner.updateProgress(progress)
        bannerP2.updateProgress(progressP2)
        if (nivelPlanet.progressLimit<progress){
            nivelPlanet.update();
        }
        if (nivelAsteroid.progressLimit<progress){
            nivelAsteroid.update();
        }
        if (nivelMissile.progressLimit<progress){
            nivelMissile.update();
        }
        if (frames % 75 === 0){
            progress++
            banner.updateProgress(progress)
        }
    }else{
        badGuy.draw();
        moveBadGuy();
        generateAsteroid(ctxP2,state,1,nivelAsteroidP2);
        drawAsteroid();
        generatePlanet(ctxP2,state,1,nivelPlanetP2,nivelMissileP2,spaceshipP2);
        drawPlanet();
        generateMissile(ctxP2,state,1,nivelMissileP2);
        drawMissile();
        drawFires();
        checkCollitions();
        checkCollitionsP2();
        checkCollitionsNaves();
        checkCollitionsNavesP2();
        drawExplosion();
        checkGameOver();
        checkWin();
        banner.updateProgress(progress)
        bannerP2.updateProgress(progressP2)
        if (nivelPlanetP2.progressLimit<progressP2){
            nivelPlanetP2.update();
        }
        if (nivelAsteroidP2.progressLimit<progressP2){
            nivelAsteroidP2.update();
        }
        if (nivelMissileP2.progressLimit<progressP2){
            nivelMissileP2.update();
        }
        if (frames % 75 === 0){
            progressP2++
            bannerP2.updateProgress(progressP2)
        }

    }
  }else if (game===0 || gameP2 ===2){
    let imagen = new Image();
    imagen.src = "imagenes/gameover.jpg";
    ctx.drawImage(imagen, 0, 0, canvas.width,canvas.height);
    imagen.src = "imagenes/winner.jpg";
    ctxP2.drawImage(imagen,0,0,canvas.height,canvas.height)
    options.style.display="block"
    options.style = "position:absolute; left: 65%; margin-left: -200px;";
  }else if (game===2 || gameP2 === 0){
    let imagen = new Image();
    imagen.src = "imagenes/winner.jpg";
    ctx.drawImage(imagen, 0, 0, canvas.width,canvas.height);
    imagen.src = "imagenes/gameover.jpg";
    ctxP2.drawImage(imagen,0,0,canvas.height,canvas.height)
    options.style.display="block"

  }  
}


/**
 *
 *
 * @param {*} context
 * @param {*} estado
 * @param {*} player
 * @param {*} [nivel=nivelAsteroid]
 */
function generateAsteroid(context,estado,player,nivel=nivelAsteroid) {
  if(frames % nivel.frame === 0){
      const randomPost = estado.generateState(nivel.width);
      const ast = new Asteroid(context,randomPost,nivel.width,nivel.height);
      asteroid[player].push(ast); 
  }
}

function drawAsteroid() {
  asteroid[0].forEach(ast => ast.draw());
  asteroid[1].forEach(ast => ast.draw());
}

function generatePlanet(context,estado,player,nivel=nivelPlanet,nivelM=nivelMissile,nave=spaceship) {
  
  if(frames % nivel.frame === 0 && frames % nivelM.frame < nivelM.frame - nave.height*2.5){
      const randomPost = estado.generateState(nivel.width);
      const plt = new Planet(context,randomPost,nivel.width,nivel.height);
      planet[player].push(plt); 
  }
}

function drawPlanet() {
  planet[0].forEach(plt => plt.draw());
  planet[1].forEach(plt => plt.draw());
}

function generateMissile(context,estado,player,nivel=nivelMissile) {
  if(frames % nivel.frame === 0){
      const randomPost = estado.generateState(nivel.width);;
      const msl = new Missile(context,randomPost,nivel.width,nivel.height);
      missile[player].push(msl); 
  }
}

function drawMissile() {
  missile[0].forEach(msl => msl.draw());
  missile[1].forEach(msl => msl.draw());
}

function checkCollitions() {
  asteroid[0].forEach((ast, i) => {
    if(spaceship.isTouching(ast,1)) {
      let explosion = new Explosion(ctx,spaceship.x + spaceship.width/8,spaceship.y + spaceship.height/8,spaceship.width/2,spaceship.height/2);
      colision.push(explosion)
      asteroid[0].splice(i, 1);
      spaceship.hp--;
      banner.updateHP();
    }
    
    if (ast.y>canvas.height){
        asteroid[0].splice(i,1)
    }
    fires.forEach((fire,j)=>{
      if (fire.isTouching(ast)){
        let explosion = new Explosion(ctx,ast.x,ast.y,fire.width,fire.height)
        colision.push(explosion)
        asteroid[0].splice(i, 1);
        fires.splice(j,1);
        progress++;
      }
    })
  })
  planet[0].forEach((plt, i) => {
    if(spaceship.isTouching(plt,1)) {
      let explosion = new Explosion(ctx,spaceship.x + spaceship.width/8,spaceship.y + spaceship.height/8,spaceship.width/2,spaceship.height/2);
      colision.push(explosion)
      planet[0].splice(i, 1);
      spaceship.hp--;
      banner.updateHP();
    }
    if (plt.y>canvas.height){
        planet[0].splice(i,1)
    }
    fires.forEach((fire,j)=>{
      if (fire.isTouching(plt)){
        let explosion = new Explosion(ctx,plt.x,plt.y,fire.width,fire.height)
        colision.push(explosion)
        planet[0].splice(i, 1);
        fires.splice(j,1);
        spaceship.hp--;
        banner.updateHP();
      }
    })
  })
  missile[0].forEach((msl, i) => {
    if(spaceship.isTouching(msl)) {
      let explosion = new Explosion(ctx,spaceship.x + spaceship.width/8,spaceship.y + spaceship.height/8,spaceship.width/2,spaceship.height/2);
      colision.push(explosion)
      missile[0].splice(i, 1);
      spaceship.hp--;
      banner.updateHP();
    }
    if (msl.y>canvas.height){
        missile[0].splice(i, 1);
        spaceship.hp--;
        banner.updateHP();
    }
    fires.forEach((fire,j)=>{
      if (fire.isTouching(msl)){
        let explosion = new Explosion(ctx,msl.x,msl.y,fire.width,fire.height)
        colision.push(explosion)
        missile[0].splice(i, 1);
        fires.splice(j,1);
      }
    })
  })
}

function checkCollitionsP2() {
    asteroid[1].forEach((ast, i) => {
      if(spaceshipP2.isTouching(ast)) {
        let explosion = new Explosion(ctxP2,spaceshipP2.x + spaceshipP2.width/8,spaceshipP2.y + spaceshipP2.height/8,spaceshipP2.width/2,spaceshipP2.height/2);
        colision.push(explosion)
        asteroid[1].splice(i, 1);
        spaceshipP2.hp--;
        bannerP2.updateHP();
      }
      if (ast.y>canvas.height){
        asteroid[1].splice(i,1)
    }
      firesP2.forEach((fireP2,j)=>{
        if (fireP2.isTouching(ast)){
          let explosion = new Explosion(ctxP2,ast.x,ast.y,fireP2.width,fireP2.height)
          colision.push(explosion)
          asteroid[1].splice(i, 1);
          firesP2.splice(j,1);
          progressP2++
        }
      })
    })
    planet[1].forEach((plt, i) => {
      if(spaceshipP2.isTouching(plt)) {
        let explosion = new Explosion(ctxP2,spaceshipP2.x + spaceshipP2.width/8,spaceshipP2.y + spaceshipP2.height/8,spaceshipP2.width/2,spaceshipP2.height/2);
        colision.push(explosion)
        planet[1].splice(i, 1);
        spaceshipP2.hp--;
        bannerP2.updateHP();
      }
      if (plt.y>canvas.height){
          planet[1].splice(i,1)
      }
      firesP2.forEach((fireP2,j)=>{
        if (fireP2.isTouching(plt)){
          let explosion = new Explosion(ctxP2,plt.x,plt.y,fireP2.width,fireP2.height)
          colision.push(explosion)
          planet[1].splice(i, 1);
          firesP2.splice(j,1);
          spaceshipP2.hp--;
          bannerP2.updateHP();
        }
      })
    })
    missile[1].forEach((msl, i) => {
      if(spaceshipP2.isTouching(msl)) {
        let explosion = new Explosion(ctxP2,spaceshipP2.x + spaceshipP2.width/8,spaceshipP2.y + spaceshipP2.height/8,spaceshipP2.width/2,spaceshipP2.height/2);
        colision.push(explosion)
        missile[1].splice(i, 1);
        spaceshipP2.hp--;
        bannerP2.updateHP();
      }
      if (msl.y>canvas.height){
        missile[1].splice(i, 1);
        spaceshipP2.hp--;
        bannerP2.updateHP();
        }
      firesP2.forEach((fireP2,j)=>{
        if (fireP2.isTouching(msl)){
          let explosion = new Explosion(ctxP2,msl.x,msl.y,fireP2.width,fireP2.height)
          colision.push(explosion)
          missile[1].splice(i, 1);
          firesP2.splice(j,1);
        }
      })
    })
}
function checkCollitionsNaves(){
  fires.forEach((fire,i)=>{
    if (fire.isTouching(badGuy,0)){
      let explosion = new Explosion(ctx,badGuy.x,badGuy.y,fire.width,fire.height)
      colision.push(explosion)
      fires.splice(i,1);
      progress+=4.2
    }
  })
  badFire.forEach((bad,j)=>{
  if (bad.isTouching(spaceship)){
    let explosion = new Explosion(ctx,spaceship.x+10,spaceship.y,spaceship.width/2,spaceship.height/2)
    colision.push(explosion)
    badFire.splice(j,1);
    spaceship.hp--;
    banner.updateHP();
  }

    })
}  
function checkCollitionsNavesP2(){
    firesP2.forEach((fire,i)=>{
      if (fire.isTouching(badGuyP2,0)){
        let explosion = new Explosion(ctxP2,badGuyP2.x,badGuyP2.y,fire.width,fire.height)
        colision.push(explosion)
        firesP2.splice(i,1);
        progressP2+=4.2
      }
    })
  badFireP2.forEach((bad,j)=>{
    if (bad.isTouching(spaceshipP2)){
      let explosion = new Explosion(ctxP2,spaceshipP2.x+10,spaceshipP2.y,spaceshipP2.width/2,spaceshipP2.height/2)
      colision.push(explosion)
      badFireP2.splice(j,1);
      spaceshipP2.hp--;
      bannerP2.updateHP();
    }
  
  })  

}
function drawExplosion(){
  colision.forEach(coli => coli.draw());
}

function moveBadGuy(){
  if (frames % 7 === 0){
    if (progressP2>=180){
      badGuyP2[moves[Math.floor(Math.random()*6)]]()
    }
    if (progress>=180){
      badGuy[moves[Math.floor(Math.random()*6)]]()
    }
    
  }
  if (frames % 11 === 0){
    if (progressP2>=180){
      badFireP2.push(new BadFire(ctxP2,badGuyP2.x + badGuyP2.width/4 + 15,badGuyP2.y + badGuyP2.height/2 -15))
    }
    if (progress>=180){
      badFire.push(new BadFire(ctx,badGuy.x + badGuy.width/4 + 15,badGuy.y + badGuy.height/2 -15))
    }
    
    
  }
  
}

function drawFires(){
  fires.forEach(fire => fire.draw());
  badFire.forEach(badF => badF.draw());
  firesP2.forEach(fireP2 => fireP2.draw());
  badFireP2.forEach(badFP2 => badFP2.draw());
}
function checkWin(){
  if (progress > 200) { 
        game=2;
        let imagen = new Image();
        let imagenP2 = new Image(); 
        imagen.src = "imagenes/winner.jpg";
        imagenP2.src = "imagenes/gameover.jpg"
        imagen.onload = function () {
        ctx.drawImage(imagen, 0, 0, canvas.width,canvas.height);
        ctxP2.drawImage(imagenP2, 0, 0, canvas.width,canvas.height);
        }

    }else if (progressP2>200){  
        gameP2=2;
        let imagen = new Image();
        let imagenP2 = new Image();
        imagen.src = "imagenes/gameover.jpg";
        imagenP2.src = "imagenes/winner.jpg"
        imagen.onload = function () {
        ctx.drawImage(imagen, 0, 0, canvas.width,canvas.height);
        ctxP2.drawImage(imagenP2, 0, 0, canvas.width,canvas.height);
        }


    }
}
function checkGameOver(){
  if (banner.score < 1) {
    let imagen = new Image();
    let imagenP2 = new Image();
    imagen.src = "imagenes/gameover.jpg";
    imagenP2.src ="imagenes/winner.jpg";
    imagen.onload = function () {
    ctx.drawImage(imagen, 0, 0, canvas.width,canvas.height);
    ctxP2.drawImage(imagenP2, 0, 0, canvas.width,canvas.height)
    }
    game=0;
  }
  if (bannerP2.score<1){
    let imagen = new Image();
    let imagenP2 = new Image();
    imagen.src = "imagenes/winner.jpg";
    imagenP2.src = "imagenes/gameover.jpg";
    imagen.onload = function () {
    ctx.drawImage(imagen, 0, 0, canvas.width,canvas.height);
    ctxP2.drawImage(imagenP2, 0, 0, canvas.width,canvas.height);
    }
    gameP2=0;

  }
}